1-leer el archivo RMDSelect.xml recuperar las lineas que contengan los value que contengan INC ej: <value>INC000019575097</value> dejando solamente el id ej: INC000019575097
hacer dos versiones utilizando distintos comandos. 

2- leer el archivo ordenes_de_trabajo.csv recuperar solamente los ticket en estado pendiente,Asignado,En curso y reordenar la salida mostrando primero el estado luego el id y por ultimo la descirpcion
ej: ESTADO:Pendiente - ID:WO0000001534022 - DESCRIPCION:TEST

3- del listado del punto anterior generar tres archivos csv con el nombre del estado y la fecha ej: ticket_pendientes_11-Oct-2022.csv


1-
cat RMDSelect.xml | grep "INC" | cut -d "<" -f 2 | cut -d ">" -f 2
cat RMDSelect.xml | grep "INC" | tr '<value>' ' ' | tr '/' ' '| tr -d ' '
cat RMDSelect.xml | grep "INC" | awk -F'>' '{print $2}'| awk -F'<' '{print $1}'
cat RMDSelect.xml | grep "INC" | sed -e 's/<value>//g'| sed -e 's/<\/value>//g'
2-
cat ordenes_de_trabajo.csv | grep -iE "Pendiente|asignado|en curso" | awk -F';' '{print "ESTADO:"$3 " - ID:"$1 " - DESCRIPCION:" $4}'|grep -v "standard input"'
cat ordenes_de_trabajo.csv | grep -iEa "Pendiente|asignado|en curso" | awk -F';' '{print "ESTADO:"$3 " - ID:"$1 " - DESCRIPCION:" $4}'
3-
cat ordenes_de_trabajo.csv | grep "Pendiente" | awk -F';' '{print "ESTADO:"$3 " ; ID:"$1 " ; DESCRIPCION:" $4}' >ticket_pendientes_$(date +'%d-%b-%y').csv
cat ordenes_de_trabajo.csv | grep "Asignado" | awk -F';' '{print "ESTADO:"$3 " ; ID:"$1 " ; DESCRIPCION:" $4}' >ticket_asignado_$(date +'%d-%b-%y').csv
cat ordenes_de_trabajo.csv | grep -a "En curso" | awk -F';' '{print "ESTADO:"$3 " ; ID:"$1 " ; DESCRIPCION:" $4}' >ticket_en_curso_$(date +'%d-%b-%y').csv
---------
