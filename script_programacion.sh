#!/bin/bash

var1="hola mundo 123"
for i in 1 2 3 4 5
do
	echo "Hello $i"
done

for i in $(ls)
do
	echo "Hola $i"
done

for (( c=1; c<=5; c++ ))
do
	echo "Hola $c"
done

echo "${var1}"
